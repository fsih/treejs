"use strict";

//Models and views related to the tree

App.BranchView = App.RaphaelView.extend({

  initialize: function() {
    this.myBranches = new Array();
    this.paper = App.paper;
    this.sprite = this.paper.set();
    this.branchSprites = [];
    this.render(this.options.model);
  },

  render: function(model) {
	this.branchSvg = model.branchSvg;
    this.draw(model);
  },

  draw: function(model) {
    this.branchBody = this.paper.path(this.branchSvg.path);
    this.branchBody.attr({
        fill: this.branchSvg.fillColor,
        stroke: this.branchSvg.strokeColor,
        'stroke-width': model.get('scale'),
    });

    for (var i = 0; i < model.myBranches.length; i++) {
      var currBunch = [];
      var currBunchSprites = this.paper.set();
      for (var j = 0; j < model.myBranches[i].bunch.length; j++) {
        var b = new App.BranchView({model: model.myBranches[i].bunch[j]});
        currBunch.push(b);
        currBunchSprites.push(b.sprite);
      }
      this.branchSprites.push(currBunchSprites);
      this.sprite.push(currBunchSprites);
      this.myBranches.push(currBunch);
    }

    this.sprite.push(this.branchBody);
  },

  doTranslate: function() {
    this.sprite.transform("..."+this.model.get('doTranslate'));
    for (var i = 0; i < this.myBranches.length; i++){
      for (var j = 0; j < this.myBranches[i].length; j++){
        var b = this.myBranches[i][j];
        b.doTranslate();
      }
    }
  },

  doRotate: function() {
    this.sprite.transform("..."+this.model.get('doRotate'));
    for (var i = 0; i < this.myBranches.length; i++){
      for (var j = 0; j < this.myBranches[i].length; j++){
        var b = this.myBranches[i][j];
        b.doRotate();
      }
    }
  },

  doScale: function() {
    for (var i = 0; i < this.myBranches.length; i++){
      for (var j = 0; j < this.myBranches[i].length; j++){
        var b = this.myBranches[i][j];
        b.sprite.transform("..."+b.model.get('doScale'));
        b.doScale();
      }
    }
  },

  // Prereq: must have called drawProjectBoxes before calling this, which means
  // this can't be called until whole tree is drawn.
  showActiveBunch: function() {
    var activeBunch = this.options.model.activeBunch;

    // Hide all but activeBunch
    for (var i = 0; i < this.myBranches.length; i++) {
      for (var j = 0; j < this.myBranches[i].length; j++){
        var b = this.myBranches[i][j];
        if (b.projectBox) {
          if (i == activeBunch) {
            b.sprite.show();
            if (b.pageturner)
              b.pageturner.update();

            // Since show() function shows all previously hidden sub-bunches, have to
            // recurse through and hide previously hidden bunches
            b.showActiveBunch();
          }
          else
            b.sprite.hide();
        }
      }
    }


    if (this.pageturner)
      this.pageturner.update();

    // If the thing with the glow on it is now hidden, remove the glow from it
    if (App.wrapper.focused != false && App.wrapper.focused.isVisible() == false) {
      App.wrapper.focused.g.remove();
      App.wrapper.focused = false;
    }  
  }

});

App.RootView = App.BranchView.extend({
  render: function(model) {
    if (this.model.myBranches.length == 0) {
      this.drawNotFoundBackdrop();
      $('#zoomContainer').remove(); // Can't zoom
      return;
    }
    this.branchSvg = model.branchSvg;
    this.drawBackdrop(); // Currently has nothing drawn on it, but we can put something in the background
    this.draw(model); // Draw this branch, which recursively draws subbranches
    this.doTranslate(); // Translate this branch, which recursively ...
    this.doScale(); // Scale this branch, ...
    this.doRotate(); // Rotate this branch, ...
    this.drawPanBox(); // Create the rect that deals with pan and scroll

    // Flip the tree over
    this.sprite.transform('r180 0 0...');

    this.drawPageturners(); // Draw page turners
    this.drawProjectBoxes(this.options.focus); // Create the project boxes
    this.showActiveBunch(); // Hide all the bunches that should be hidden
  },

  drawBackdrop: function() {
    App.wrapper.backdrop = new App.Backdrop();
  },

  drawNotFoundBackdrop: function() {
    App.wrapper.backdrop = new App.NotFoundBackdrop();
  },

  drawPanBox: function() {
    App.wrapper.panBox = new App.PanBox();
  },

  drawProjectBoxes: function(focus) {
    var branches = new Array();
    branches.push(this);
    while(branches.length>0){
      var b = branches.pop();

      for (var i = 0; i < b.myBranches.length; i++){
        for (var j = 0; j < b.myBranches[i].length; j++){
          branches.push(b.myBranches[i][j]);
        }
      }

      b.projectBox = new App.ProjectView({branch: b, focus: focus});
      b.sprite.push(b.projectBox.sprite);
    }
  },

  drawPageturners: function(focus) {
    var leaves = new Array();
    leaves.push(this);
    while(leaves.length>0){
      var b = leaves.pop();

      for (var i = 0; i < b.myBranches.length; i++){
        for (var j = 0; j < b.myBranches[i].length; j++){
          leaves.push(b.myBranches[i][j]);
        }
      }

      if (b.myBranches.length > 1) {
        b.pageturner = new App.Pageturner({branch: b});
        b.sprite.push(b.pageturner.sprite);
      }
    }
  },
});

App.BranchModel = Backbone.Model.extend({
    debug: false,
    angleL: 0,
    angleR: 0,
    childAngleL: 0,
    childAngleR: 0,
    bunchNum: 0, // My parent's index for the bunch I'm in
    activeBunch: 0, // My index for the bunch of children I'm showing
    numChildren: 0, // Num descendants of this node

    //Position this branch's down hinge on its parents up hinge
    positionX: function() {
      return this.get('parentModel').upHingeX();
    },
    positionY: function() {
      return this.get('parentModel').upHingeY();
    },
    absPositionX: function() {
      return this.positionX()+this.get('parentModel').absPositionX();
    },
    absPositionY: function() {
      return this.positionY()+this.get('parentModel').absPositionY();
    },

    upHingeX: function() {
      var a = Raphael.rad(this.absAngle);
      return (this.branchSvg.upHingeX-this.branchSvg.upHingeY*Math.sin(a))*this.get('scale');
    },
    upHingeY: function() {
      var a = Raphael.rad(this.absAngle);
      return this.branchSvg.upHingeY*Math.cos(a)*this.get('scale');
    },
    effectiveHeight: function() {
      return this.branchSvg.upHingeY*this.get('scale');
    },
    effectiveWidth: function() {
      return this.branchSvg.upHingeX*this.get('scale');
    },
    totalChildAngle: function() {
      return this.childAngleL + this.childAngleR;
    },

    addNewBranch: function(content, scale) {
      var newbranch = new App.ChildModel({parentModel:this, content:content, scale:scale});
      return newbranch;
    },

    childAngleToAngle: function(childAngle) {
      if (childAngle >= 90){
        return Raphael.deg(Math.atan(App.branchSvg.falloff));
      }

      if (childAngle < 0){
        throw "Angles should be non-negative";
      }

      var childRad = Raphael.rad(childAngle);
      var opp = Math.abs(Math.sin(childRad)); // o/h
      var adj = Math.cos(childRad); // a/h
      opp *= this.effectiveHeight(); // o/h * h = o = O
      adj *= this.effectiveHeight(); // a/h * h = a
      adj += this.effectiveHeight(); // a + a' = A
      var ans = Math.atan(opp/adj); // atan(O/A) = new angle

      return Raphael.deg(ans);
    },

    //For debugging
    circleThisPoint: function(x,y,strokeColor) {
      if(!this.debug)
        return;

      var circ = this.paper.circle(x,y,6);
      circ.attr({
          stroke: strokeColor,
      });
      this.sprite.push(circ);
    },

    //For debugging
    printPosition: function() {
      this.dlog("Position "+this+": "+this.positionX()+","+this.positionY());
    },

    //For debugging
    dlog: function(message) {
      if(!this.debug)
        return;

      console.log(message);
    },

    setup: function(content) {
        //children

        this.dlog("vvvvvvvvvvvvvvvv");
        var childL = 0;
        var childR = 0;
        var currChildL = 0;
        var currChildR = 0;
        var myBranches = [];
        var currBunch = [];
        this.myBranches = myBranches;
        this.dlog("content length"+content.children.length);
        this.projectsToLeft = [0]; // Num projects to left of each bunch
        for (var i = 0; i < content.children.length; i++){
          var b = this.addNewBranch(content.children[i], this.get('scale')*this.branchSvg.falloff);

          // Don't exceed a certain angle per node per bunch, but require at least one branch per bunch
          if (currBunch.length > 0 && currChildL + b.angleL + currChildR + b.angleR > this.branchSvg.maxChildAngle) {
            myBranches.push({bunch: currBunch, childL: currChildL, childR: currChildR});
            this.projectsToLeft.push(this.numChildren); // Pushes to the n+1th spot when we have numChildren up to nth branch
            currBunch = [];
            if (childL < currChildL)
              childL = currChildL;
            if (childR < currChildR)
              childR = currChildR;
            currChildL = currChildR = 0;
          }

          currChildL += b.angleL;
          currChildR += b.angleR;
          b.bunchNum = myBranches.length;
          currBunch.push(b);
          this.numChildren += b.numChildren + 1;
        }

        myBranches.push({bunch: currBunch, childL: currChildL, childR: currChildR});

        if (childL < currChildL)
          childL = currChildL;
        if (childR < currChildR)
          childR = currChildR;
        currChildR = currChildL = 0;

        //angle
        this.childAngleL = childL;
        this.childAngleR = childR;
        this.angleL = Math.max(this.branchSvg.minAngle/2, this.childAngleToAngle(childL));
        this.angleR = Math.max(this.branchSvg.minAngle/2, this.childAngleToAngle(childR));
        this.dlog('angles:'+this.angleL+" "+this.angleR);
        this.dlog('childangles:'+childL+" "+childR);
        for (var i = 0; i < myBranches.length; i++) {
          var offset = -myBranches[i].childL;
          this.dlog("bunches length "+myBranches.length);
          for (var j = 0; j < myBranches[i].bunch.length; j++){
            var b = myBranches[i].bunch[j];
            offset += b.angleL;
            b.offset = offset;
            offset += b.angleR;
            this.dlog("set offset "+b.offset);
          }
        }
        this.dlog("^^^^^^^^^^^^^^^^");
    },

    calculateAngles: function() {
        for (var i = 0; i < this.myBranches.length; i++){
          for (var j = 0; j < this.myBranches[i].bunch.length; j++){
            var b = this.myBranches[i].bunch[j];
            b.absAngle = this.absAngle+b.offset;
            b.calculateAngles();
          }
        }
    },

    calculateTranslate: function() {
      for (var i = 0; i < this.myBranches.length; i++){
        for (var j = 0; j < this.myBranches[i].bunch.length; j++){
          var b = this.myBranches[i].bunch[j];
          b.calculateTranslate();
        }
      }
      this.set('doTranslate', "t"+this.positionX()+","+this.positionY());
    },

    calculateRotate: function() {
      this.set('doRotate', "r"+this.offset+",0,0");

      for (var i = 0; i < this.myBranches.length; i++){
        for (var j = 0; j < this.myBranches[i].bunch.length; j++){
          var b = this.myBranches[i].bunch[j];
          b.calculateRotate();
        }
      }
    },

    calculateScale: function() {
      for (var i = 0; i < this.myBranches.length; i++){
        for (var j = 0; j < this.myBranches[i].bunch.length; j++){
          var b = this.myBranches[i].bunch[j];
          b.set('doScale', "s"+this.branchSvg.falloff+","+this.branchSvg.falloff+",0,0");
          b.calculateScale();
        }
      }
    },

});

App.ChildModel = App.BranchModel.extend({

    initialize: function() {
	    this.branchSvg = App.branchSvg;
        //draw
        this.setup(this.get('content'));
    },
});

App.RootModel = App.BranchModel.extend({

    //Bounding box properties
    rootX:0,
    rootY:0,

    //position of 0,0 of the root
    positionX: function() {
      return -this.rootX;
    },
    positionY: function() {
      return -this.rootY;
    },
    absPositionX: function() {
      return this.positionX()
    },
    absPositionY: function() {
      return this.positionY()
    },

    initialize: function() { 

	  this.branchSvg = App.branchSvg;
	
      //setup children
      this.set('scale', 1);
      this.setup(this.get('content'));

      this.offset=0;
      this.absAngle=0;

      //calculate positions and transformations for children
      this.calculateAngles();
      this.calculateBounds();
      this.calculateTranslate();
      this.calculateScale();
      this.calculateRotate();

    },

    calculateBounds: function() {
      //Get the bounding box of the tree
      var boundX = 0;
      var boundnX = 0;
      var boundY = 0;
      var leaves = new Array();
      leaves.push(this);
      while(leaves.length>0){
        var b = leaves.pop();
        for (var i = 0; i < b.myBranches.length; i++){
          for (var j = 0; j < b.myBranches[i].bunch.length; j++){
            leaves.push(b.myBranches[i].bunch[j]);
          }
        }

        if (b.absPositionX()-this.absPositionX()+b.upHingeX()>boundX)
          boundX = b.absPositionX()-this.absPositionX()+b.upHingeX();

        if (b.absPositionX()-this.absPositionX()+b.upHingeX()<boundnX)
          boundnX = b.absPositionX()-this.absPositionX()+b.upHingeX();

        if (b.absPositionY()-this.absPositionY()+b.upHingeY()>boundY)
          boundY = b.absPositionY()-this.absPositionY()+b.upHingeY();

      }

      this.rootX = boundX+App.wrapper.margins;
      this.rootY = boundY+App.wrapper.margins;

      var width = boundX-boundnX+2*App.wrapper.margins;
      var height = boundY+App.wrapper.margins+App.wrapper.bottomMargin;

      this.setCorrectProportions(width,height);
    },

    setCorrectProportions: function(width, height) {
      // Make same proportion as window it is viewed from, so when you zoom all the way
      // out the background covers everything
      if (width/height > App.wrapper.minWidth/App.wrapper.minHeight) {
        // Width is too big so buffer height
        var newHeight = width*App.wrapper.minHeight/App.wrapper.minWidth;
        this.rootY += (newHeight-height); //buffer all on top, so that the tree is still "grounded"
        height = newHeight;
      } else if (width/height < App.wrapper.minWidth/App.wrapper.minHeight) {
        // Width is too small, buffer width
        var newWidth = height*App.wrapper.minWidth/App.wrapper.minHeight;
        this.rootX += (newWidth-width)/2; //buffer half on top, half on bottom
        width = newWidth;
      }

      // Make sure canvas is at least as big as viewing window
      if (width < App.wrapper.minWidth) {
        var newWidth = App.wrapper.minWidth;
        var newHeight = App.wrapper.minHeight;
        this.rootY += (newHeight-height); //buffer all on top
        this.rootX += (newWidth-width)/2; //buffer half on top, half on bottom
        height = newHeight;
        width = newWidth;
      }

      App.wrapper.absWidth = Math.ceil(width);
      App.wrapper.absHeight = Math.ceil(height);
      App.wrapper.render();
    }


});

//Basic TreeView
//It is referenced from index.html
App.TreeView = Backbone.View.extend({
  initialize:function() {
    this.render();
  },

  render:function() {
    this.el.id = 'treeView';
    $("#tree").html("");
    $("#tree").append(this.el);
    return this;
  },

});