"use strict";

App.ProjectView = App.RaphaelView.extend({
  initialize: function() {
    this.paper = App.paper;
    this.sprite = App.paper.set();
    this.render(this.options.branch.model, this.options.focus);
  },

  render: function(b, focus) {
    var that = this;
    var posx = -b.absPositionX()-b.upHingeX();
    var posy = -b.absPositionY()-b.upHingeY();
    var scale = b.get('scale');
    var w = App.projectBoxSvg.w;
    var h = App.projectBoxSvg.h;
    var puffFactor = App.projectBoxSvg.puffFactor;
    var glowColor = App.projectBoxSvg.glowColor;
    var authTextFontSize = App.projectBoxSvg.authTextFontSize;
    var projTextFontSize = App.projectBoxSvg.projTextFontSize;
    var textColor = App.projectBoxSvg.textColor;

    var creator;
    var imageLoc;
    var projectid;
    var title;
    var projectVisible;
    if (b.get('id')) {
      var c = b.get('content')[b.get('id')];
      projectid = b.get('id');
      title = c.title;
      creator = c.creator;
      projectVisible = (c.visibility == 'visible' && c.is_published != false);
      if (projectVisible) {
        if (!creator)
          creator = c.username; // Mistake in db makes it referred to as either, should be fixed.
        if (projectid > 10000000) {
          var s = projectid.toString();
          imageLoc = "http://beta.scratch.mit.edu/static/site/projects/thumbnails/"+s.substr(0,4)+"/"+s.substr(4)+".png"
        } else {
          // This will be fixed when old projects are ported to the new remix db
          imageLoc = "http://scratch.mit.edu/static/projects/"+creator+"/"+projectid+"_sm.png";
        }
      } else {
        imageLoc = static_url+"/treejs/img/private_cat.png";
      }
    } else {
      var c = b.get('content');
      projectid = c.data.pid;
      title = c.name;
      creator = c.data.username;
      // Visibility is not currently dealt with on older projects, because the old db doesn't store visibility data.
      projectVisible = true;
      if (projectVisible) {
        imageLoc = "http://scratch.mit.edu/static/projects/"+creator+"/"+projectid+"_sm.png"
      } else {
        imageLoc = static_url+"/treejs/img/private_cat.png";
      }
    }

    var sprite = this.sprite;

    var projectBox = this.paper.image(imageLoc);
    projectBox.attr({
      x:-w/2*scale+posx,
      y:-h/2*scale+posy,
      width:w*scale,
      height:h*scale,
    });
    var outline = this.paper.rect();
    outline.attr({
      x:-w/2*scale+posx,
      y:-h/2*scale+posy,
      width:w*scale,
      height:h*scale,
      r:2*scale,
      // 'stroke-width': scale,
      'stroke':App.projectBoxSvg.strokeColor,
      'fill':App.projectBoxSvg.fillColor,
      'fill-opacity':0,
    });
    if (projectVisible) {
      outline.attr({
        href: "http://beta.scratch.mit.edu/projects/"+projectid+"/"
      });
    }
    this.sprite.push(projectBox);
    this.sprite.push(outline);

    var dupBox;
    var dupOutline;
    var detailBoxPadding = App.projectBoxSvg.detailBoxPadding;
    var projectName;
    var authorName;
    var container;
    var hovered = false;
    var loveit;
    var loveText;
    var fave;
    var faveText;

    var hoverIn = function() {
      // Don't activate hover while dragging
      if (App.wrapper.draggingActive == true)
          return;

      // Don't activate if already hovering over
      if (hovered == true)
        return;
      hovered = true;

      // Set to contain popup
      var detailBox = App.paper.set();

      // Remove glow from previous focus and apply new glow
      if (App.wrapper.focused != false) {
        App.wrapper.focused.g.remove();
      }      
      projectBox.g = projectBox.glow({
        color: glowColor,
        width: 20,
        opacity: 1
      });
      App.wrapper.focused = projectBox;

      // Close previous hover if for some reason it's still open and set this to hoverActive
      if (App.wrapper.hoverActive != false) {
        App.wrapper.hoverActive[1].inBounds = false; // Fix hoverInBounds state if hoverInBounds.js is in use
        App.wrapper.hoverActive[0].hoverOut(); // Run callback; close last box
      }
      App.wrapper.hoverActive = [that,this];

      // Create detail box components
      dupBox = projectBox.clone(); // Instead of changing index of actual project box, clone it
      dupOutline = outline.clone(); // Same for outline

      // If project visible, show author and title. 
      var authorText = projectVisible ? "by "+creator : "This project is not shared.";
      var titleText = projectVisible ? title : " ";
      authorName = this.paper.print(0,0,authorText,this.paper.getFont("NotoSans"),authTextFontSize/App.wrapper.scale,"baseline");
      projectName = this.paper.print(0,0,titleText,this.paper.getFont("NotoSans", 700, "bold"),projTextFontSize/App.wrapper.scale,"baseline");
    
      //shorten text to fit in box
      var i = authorText.length;
      while ((authorName.getBBox().width > w*puffFactor/App.wrapper.scale) && i > 0) {
        i--;
        var currentText = authorText.substring(0,i)+"…";
        authorName.remove();
        authorName = this.paper.print(0,0,currentText,this.paper.getFont("NotoSans"),authTextFontSize/App.wrapper.scale,"baseline");;
      }
      i = titleText.length;
      while ((projectName.getBBox().width > w*puffFactor/App.wrapper.scale) && i > 0) {
        i--;
        var currentText = titleText.substring(0,i)+"…";
        projectName.remove();
        projectName = this.paper.print(0,0,currentText,this.paper.getFont("NotoSans", 700, "bold"),projTextFontSize/App.wrapper.scale,"baseline");
      }

      var authX = posx - authorName.getBBox().x - authorName.getBBox().width/2;
      var authY = posy - authorName.getBBox().y - h*puffFactor/2/App.wrapper.scale - authorName.getBBox().height - 2/5*detailBoxPadding/App.wrapper.scale;
      var projX = posx - projectName.getBBox().x - projectName.getBBox().width/2;
      var projY = authY - projectName.getBBox().height - 1/5*detailBoxPadding/App.wrapper.scale;
      authorName.transform("t"+authX+","+authY);
      projectName.transform("t"+projX+","+projY);
      authorName.attr('fill',textColor);
      projectName.attr('fill',textColor);

      // Project stats
      // Show project stats if loves and faves aren't both 0
      var showProjectStats = (c.hasOwnProperty('love_count') && c.hasOwnProperty('favorite_count') && (c.love_count > 0 || c.favorite_count > 0));
      // Don't show project stats if project not visible
      showProjectStats = projectVisible ? showProjectStats : false;
      if (showProjectStats) {
        fave = this.paper.path(App.faveSvg.path);
        fave.attr({
          fill:"#484848",
          'stroke-opacity':0
        });
        var favX = posx - w*puffFactor/2/App.wrapper.scale;
        var favY = posy + h*puffFactor/2/App.wrapper.scale + detailBoxPadding/2;
        fave.transform("...t"+favX+","+favY);
        fave.transform("...s"+1/App.wrapper.scale+","+1/App.wrapper.scale+",0,0");
        faveText = this.paper.print(0,0,c.favorite_count,this.paper.getFont("NotoSans"),authTextFontSize/App.wrapper.scale,"baseline");
        var faveTextX = favX+fave.getBBox().width;
        var faveTextY = favY+(fave.getBBox().height+faveText.getBBox().height)/2;
        faveText.transform("t"+faveTextX+","+faveTextY);

        loveit = this.paper.path(App.loveitSvg.path);
        loveit.attr({
          fill:"#484848",
          'stroke-opacity':0
        });
        var lovX = favX + fave.getBBox().width+faveText.getBBox().width + detailBoxPadding*3;
        var lovY = favY;
        loveit.transform("...t"+lovX+","+lovY);
        loveit.transform("...s"+1/App.wrapper.scale+","+1/App.wrapper.scale+",0,0");
        loveText = this.paper.print(0,0,c.love_count,this.paper.getFont("NotoSans"),authTextFontSize/App.wrapper.scale,"baseline");
        loveText.transform("t"+(lovX+loveit.getBBox().width)+","+faveTextY);
      }

      // Extra space to leave to show stats
      var statsSpace;
      if (showProjectStats) 
        statsSpace = Math.max(loveit.getBBox().height, loveText.getBBox().height);
      else 
        statsSpace = 0;

      // While dupeOutline plays the old role of outline, outline
      // transparently covers the entire popup to allow it to act as one link
      // and hover item. This trick is necessary to detect hover events accurately
      // on the popup, but means we can only have 1 hyperlink, and outline's node
      // does get and remain reordered.
      outline.attr({
        x:posx-w*puffFactor/2/App.wrapper.scale-detailBoxPadding/App.wrapper.scale,
        y:posy-h*puffFactor/2/App.wrapper.scale-projectName.getBBox().height-authorName.getBBox().height-detailBoxPadding/App.wrapper.scale,
        width:w*puffFactor/App.wrapper.scale+2*detailBoxPadding/App.wrapper.scale,
        height:h*puffFactor/App.wrapper.scale+projectName.getBBox().height+authorName.getBBox().height+2*detailBoxPadding/App.wrapper.scale+statsSpace,
        //'stroke-width': 1,
        r:5/App.wrapper.scale
      });

      // In case the popup is smaller than the original project box (if you're
      // at high zoom), hide the original box.
      projectBox.hide();
      projectBox.g.hide();

      // Container is the white background to the popup
      container = outline.clone();
        
      // Bring group forward
      dupBox.toFront();
      dupOutline.toFront();
      projectName.toFront();
      authorName.toFront();
      detailBox.push(dupBox);
      detailBox.push(dupOutline);
      detailBox.push(projectName);
      detailBox.push(authorName);
      detailBox.push(container);

      if (showProjectStats) {
        loveit.toFront();
        fave.toFront();
        loveText.toFront();
        faveText.toFront();
        detailBox.push(loveit);
        detailBox.push(fave);
        detailBox.push(loveText);
        detailBox.push(faveText);
      }
      
      // Outline is the event catcher so it should be on top
      outline.toFront();
      detailBox.push(outline);

      // Make sure it's not falling off the canvas. Prefer to align top left
      if (outline.attr('y') + container.attr('height') > App.wrapper.viewBox.yy + App.wrapper.minHeight/App.wrapper.scale) {
          detailBox.transform('...t0,'+(App.wrapper.viewBox.yy + App.wrapper.minHeight/App.wrapper.scale - container.attr('height') - outline.attr('y')));
      }      
      if (outline.attr('x') + container.attr('width') > App.wrapper.viewBox.xx + App.wrapper.minWidth/App.wrapper.scale) {
          detailBox.transform('...t'+(App.wrapper.viewBox.xx + App.wrapper.minWidth/App.wrapper.scale - container.attr('width') - outline.attr('x'))+',0');
      }
      if (outline.attr('y') < App.wrapper.viewBox.yy) {
        detailBox.transform('...t0,'+(-outline.attr('y')+App.wrapper.viewBox.yy));
      }
      if (outline.attr('x') < App.wrapper.viewBox.xx) {
        detailBox.transform('...t'+(-outline.attr('x')+App.wrapper.viewBox.xx)+',0');
      }

      dupBox.animate({        
        x:posx-w*puffFactor/2*1/App.wrapper.scale,
        y:posy-h*puffFactor/2*1/App.wrapper.scale,
        width:w*puffFactor*1/App.wrapper.scale,
        height:h*puffFactor*1/App.wrapper.scale,
      }, 100);

      dupOutline.animate({
        x:posx-w*puffFactor/2*1/App.wrapper.scale,
        y:posy-h*puffFactor/2*1/App.wrapper.scale,
        width:w*puffFactor*1/App.wrapper.scale,
        height:h*puffFactor*1/App.wrapper.scale,
      }, 100);

      container.animate({
        'fill-opacity':1
      }, 250);

      container.g = container.glow({
        color: glowColor,
        width: 20,
        opacity: 1
      });
    };

    var hoverOut = function() {
      if (hovered == false)
        return;

      hovered = false;
      projectName.remove();
      authorName.remove();
      dupOutline.remove();
      dupBox.remove();
      // For some reason the glow doesn't get removed when project is not visible
      if (!projectVisible) container.g.remove();
      container.remove();
      if (loveit) loveit.remove();
      if (fave) fave.remove();
      if (loveText) loveText.remove();
      if (faveText) faveText.remove();
      projectBox.show();
      projectBox.g.show();
      outline.transform('T0,0');
      outline.animate({
        x:-w/2*scale+posx,
        y:-h/2*scale+posy,
        width:w*scale,
        height:h*scale,
       //'stroke-width': scale,
        r:2*scale
      }, 150);
      App.wrapper.hoverActive = false;
    };
    this.hoverOut = hoverOut;

    outline.hover( hoverIn, hoverOut );

    // If this is the project whose pid is in the url
    if (projectid == focus) {
      // Make it automatically have glow and focus
      projectBox.g = projectBox.glow({
        color: glowColor,
        width: 20,
        opacity: 1
      });
      App.wrapper.focused = projectBox;

      // Zoom in on it
      App.wrapper.panBox.zoomFixedPoint(posx, posy, 1/b.get('scale'), 300);

      // Make this the active bunch so that when you first see the remix tree, the 
      // project you navigated from is visible in the tree.
      var parent = b.get('parentModel');
      while (parent != null) {
        parent.activeBunch = b.bunchNum;
        b = parent;
        parent = parent.get('parentModel');
      }

    }
  },
});