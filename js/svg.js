"use strict";

/*
  SVG paths
*/

// Dimensions and properties of project preview boxes
App.projectBoxSvg = {
  w: 60,
  h: 45,
  puffFactor: 2.5,
  detailBoxPadding: 5,
  glowColor: "#FBA939",
  strokeColor: "#aaaaaa",
  fillColor: "#fffefa",
  textColor: "#484848",
  projTextFontSize: 12,
  authTextFontSize: 12
};

App.loveitSvg = {
  path: "M7.888,14.341c0,0-9.054-6.144-7.761-10.832C0.929,0.6,2.449,0,3.942,0c2.653,0,3.946,1.892,3.946,1.892h0.001c0,0,1.293-1.892,3.945-1.892c1.493,0,3.013,0.6,3.815,3.509C16.942,8.197,7.889,14.341,7.888,14.341L7.888,14.341z"
};

App.faveSvg = {
  path: "M5.433,5.386 L0,5.724 LL4.197,9.19 L2.839,14.461 L7.433,11.542 L12.027,14.461 L10.669,9.19 L14.866,5.723 L9.433,5.386 L7.433,0.323 L5.433,5.386z"
};

// Clouds for background
App.bigCloudSvg = {
  path: "M463.866,166.438H875.03c0,0-37.247-1.564-57.618-4.211c-6.481-0.842-9.682-1.949-9.682-1.949l26.459-0.438c0,0-18.918-2.596-24.78-5.351c-2.876-1.352-3.589-4.027-3.589-4.027s5.079-0.55,6.507-4.656c1.388-3.991-2.919-13.503-8.034-14.294c-12.175-1.883-15.239,6.109-15.239,6.109s14.43-19.235-0.987-26.878c-11.009-5.458-18.799,2.911-18.799,2.911s-32.485,18.182-47.018,4.061c-27.102-26.337,3.489-58.581,25.976-43.449c7.156,4.814,6.846,20.691-1.54,26.746c-9.926,7.166-16.299-4.338-13.378-7.362c2.813-2.912,5.943,6.252,10.579,0.683c4.113-4.941,2.246-15.241-10.11-15.32c-10.03-0.064-17.101,17.52-11.82,27.564c8.706,16.562,34.07,7.168,43.086-2.051c18.854-19.28,20.098-47.934-13.426-61.097c-52.546-20.632-66.136,36.837-66.136,36.837s-14.067-11.35-25.333,5.832c-8.96,13.667,8.725,27.438,8.725,27.438s-4.339-7.176-13.673-8.234c-6.315-0.716-11.832,6.922-11.832,6.922s2.683-6.313,0.058-11.102c-3.408-6.216-13.555-10.927-27.404-6.878c-22.009,6.434-25.382,19.127-22.979,26.858c1.657,5.33,7.019,7.3,7.019,7.3s-8.653,3.913-9.897,9.973c-1.018,4.958,5.328,12.286,5.328,12.286s-3.604-3.695-6.869-3.195c-2.65,0.405-5.09,5.025-5.09,5.025s0.502-11.497-11.192-10.495c-8.216,0.705-11.394,6.346-10.883,10.039c0.428,3.092,4.001,4.39,4.001,4.39s-7.523,3.523-22.021,4.953C513.349,164.34,463.866,166.438,463.866,166.438z"
};

App.smallCloudSvg = {
  path: "M95.895,211.617h277.031c0,0-52.247-1.429-76.458-3.76c-7.355-0.708-11.86-4.743-11.86-4.743s14.862-5.885,6.302-17.763c-9.417-13.066-18.373,6.943-18.373,6.943s0.32,4.553-7.306,6.417c-8.676,2.121-13.814-9.206-10.561-13.295c2.667-3.352,7.807-7.901,12.329-3.388c1.571,1.567,0.181,4.942-3.159,7.42c-1.18,0.875,2.016,0.71,0.601-3.431c-0.757-2.216-4.341-2.715-6.438,1.688c-1.837,3.857,9.184,12.812,14.258-1.91c4.282-12.422,4.704-26.305-20.312-24.946c-8.616,0.468-16.442,11.089-17.426,18.806c-1.003,7.873,4.93,12.926,4.93,12.926s-5.208-8.295-11.563-6.359c-7.511,2.288-8.084,13.412-8.084,13.412s-6.104-16.269-16.016-12.172c-7.518,3.106-6.874,12.316-6.874,12.316l-13.036,3.673c0,0,7.006-3.861,7.155-7.484c0.209-5.061-7.31-10.484-16.074-11.589c-6.942-0.875-9.992,4.177-11.139,7.374c-2.177,6.07,0.256,10.589,0.256,10.589s-1.062-8.767-9.974-6.829c-9.88,2.149-4.801,9.678-4.801,9.678s0.295,1.863-8.679,2.942C124.89,210.027,95.895,211.617,95.895,211.617z"
};

// Currently unused
App.leafSvg = {
  path: "M-0.417,24.541c0,0-25.833-24.541,0-24.541C27.292,0-0.417,24.541-0.417,24.541z",
  fillColor: "#5B8A15",
  strokeColor: "#7DBF1B",
  hoverFillColor: "#7DBF1B",
  hoverStrokeColor: "#ccff99",
};

// Connect nodes
App.branchSvg = {};

// Page turning elements, which look like branches but don't move
App.turnerSvg = {};
// Own properties
App.turnerSvg.fillColor = "#ffffff";
App.turnerSvg.fillOpacity = 0.5;
App.turnerSvg.strokeColor = "#aaaaaa";
App.turnerSvg.textColor = "#484848";
App.turnerSvg.strokeDashArray = "--";
App.turnerSvg.hoverFillColor = "#cccccc";
App.turnerSvg.hoverStrokeColor = "#aaaaaa";
App.turnerSvg.boxOffsetX = 3;
App.turnerSvg.boxOffsetY = 5;
App.turnerSvg.puffFactor = 2;
App.turnerSvg.padding = 5;

App.updateForVersion = function() {
  if (App.version == 1) {
    App.branchSvg = {
      path: "M-5.205,7.994c1.236,156.528,2.043,159.045,1.807,194.989c-0.11,3.668-0.105,3.256-0.442,5.813c-0.277,2.095,1.051,4.354,4.159,4.343c3.229-0.011,4.134-2.569,4.128-4.37c-0.006-1.646-0.474-1.961-0.71-5.964c0.317-60.67-0.706-60.502,0.726-194.844L-5.205,7.994z",
      fillColor: "#484848",//"#6B4B0F", //Used to be brown
      strokeColor: "#484848",//"#6B4B0F",
      upHingeX: 0,
      upHingeY: 200,
      minAngle: 22,
      maxChildAngle: 160,
      falloff: .85,
    };
  } else if (App.version == 0) {
    App.branchSvg = {
	  path: "M-5.205,7.994c1.236,156.528,2.043,159.045,1.807,194.989c-0.11,3.668-0.105,3.256-0.442,5.813c-0.277,2.095,1.051,4.354,4.159,4.343c3.229-0.011,4.134-2.569,4.128-4.37c-0.006-1.646-0.474-1.961-0.71-5.964c0.317-60.67-0.706-60.502,0.726-194.844L-5.205,7.994z",
	  fillColor: "#cccccc",//"#6B4B0F", //Used to be brown
	  strokeColor: "#aaaaaa",//"#6B4B0F",
	  upHingeX: 0,
	  upHingeY: 200,
	  minAngle: 22,
	  maxChildAngle: 160,
	  falloff: .85,
    };
  }
  // turnerSvg "extends" branchSvg
  for (var key in App.branchSvg) {
    if (!App.branchSvg.hasOwnProperty(key))
      continue;
    if (!App.turnerSvg.hasOwnProperty(key))
      App.turnerSvg[key] = App.branchSvg[key];
  }
}