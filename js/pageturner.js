"use strict";

App.Pageturner = App.RaphaelView.extend({

  padding: App.turnerSvg.padding,
  svg: App.turnerSvg,
  disable: false,
  leftHover: false,
  rightHover: false,

  initialize: function() {
    this.paper = App.paper;
    this.sprite = App.paper.set();
    this.b = this.options.branch.model;
    this.bView = this.options.branch;
    this.render();
  },

  render: function() {
    var thiss = this;
    var b = this.b;
    var posx = -b.absPositionX()-b.upHingeX();
    var posy = -b.absPositionY()-b.upHingeY();
    this.posx = posx;
    this.posy = posy;
    var svg = this.svg;
    var scale = b.get('scale')*svg.falloff;
    var puffFactor = svg.puffFactor;

    var sprite = this.sprite;
    this.leftLeaf = this.paper.path(svg.path);
    this.leftLeaf.attr({
      fill: svg.fillColor,
      'fill-opacity': svg.fillOpacity,
      'stroke-dasharray': svg.strokeDashArray,
      stroke: svg.strokeColor,
    //  'stroke-width': scale
    });
    this.leftLeaf.transform('t'+posx+','+posy);
    this.leftLeaf.transform('...s'+scale+','+scale+',0,0');

    this.rightLeaf = this.leftLeaf.clone();

    var leafangle = (b.totalChildAngle()+b.branchSvg.minAngle)/2;
    this.leftLeaf.transform('...r'+(b.absAngle+180-leafangle)+',0,0');
    this.sprite.push(this.leftLeaf);
    this.rightLeaf.transform('...r'+(b.absAngle-180+leafangle)+',0,0');
    this.sprite.push(this.rightLeaf);

    this.leftBoxes = this.paper.set();
    this.rightBoxes = this.paper.set();
    var w = App.projectBoxSvg.w;
    var h = App.projectBoxSvg.h;
    this.w = w*scale;
    this.h = h*scale;
    this.box1 =  this.paper.rect();
    this.box1.attr({
      x:-w/2*scale+posx,
      y:-h/2*scale+posy,
      width:w*scale,
      height:h*scale,
      r:2*scale,
      //'stroke-width': scale,
      'fill':svg.fillColor,
	  'stroke': svg.strokeColor
    });

    this.box4 = this.box1.clone();
    this.box1.transform('...t'+(svg.upHingeY*scale)*Math.cos(Raphael.rad(b.absAngle-90+leafangle))+','+(svg.upHingeY*scale)*Math.sin(Raphael.rad(b.absAngle-90+leafangle)));
    this.box2 = this.box1.clone();
    this.box1.transform('...t'+(-svg.boxOffsetX*scale)+','+(-svg.boxOffsetY*scale));
    this.box3 = this.box2.clone();
    this.box3.transform('...t'+(svg.boxOffsetX*scale)+','+(svg.boxOffsetY*scale));
    this.rightText = this.paper.set();
    this.rightTransform = this.box3.transform().toString();

    this.rightBoxes.push(this.box1);
    this.rightBoxes.push(this.box2);
    this.rightBoxes.push(this.box3);
    this.rightBoxes.push(this.rightText);

    this.box4.transform('...t'+(svg.upHingeY*scale)*Math.cos(Raphael.rad(b.absAngle-90-leafangle))+','+(svg.upHingeY*scale)*Math.sin(Raphael.rad(b.absAngle-90-leafangle)));
    this.box5 = this.box4.clone();
    this.box4.transform('...t'+(-svg.boxOffsetX*scale)+','+(-svg.boxOffsetY*scale));
    this.box6 = this.box5.clone();
    this.box6.transform('...t'+(svg.boxOffsetX*scale)+','+(svg.boxOffsetY*scale));
    this.leftText = this.paper.set();
    this.leftTransform = this.box6.transform().toString();
	
    this.box3.node.setAttribute("class","clickable");
    this.box6.node.setAttribute("class","clickable");

    this.leftBoxes.push(this.box4);
    this.leftBoxes.push(this.box5);
    this.leftBoxes.push(this.box6);
    this.leftBoxes.push(this.leftText);

    sprite.push(this.rightBoxes);
    sprite.push(this.leftBoxes);

    var leftClickHandler = function(e) {
      var b = thiss.b;

      // Do nothing if left click shouldn't be possible
      if (b.activeBunch == 0 || thiss.disable) {
        return;
      }

      // Disable turning until animation is over
      thiss.disable = true;

      // Clone b's current branches, animate them
      var sprite = thiss.bView.branchSprites[b.activeBunch];
      var copy = sprite.clone();
      var rotation = 30;
      var steps = 1;
      var duration = 400;
      doRotationAnimation(copy, rotation, 0, steps, duration, function() {copy.remove()});
      
      // Hide b's branches
      thiss.bView.branchSprites[b.activeBunch].hide();

      // Switch to b's next branches, clone and animate them. At the end of animation show real branch.
      b.activeBunch--;
      thiss.bView.showActiveBunch();
      sprite = thiss.bView.branchSprites[b.activeBunch];
      var copy2 = sprite.clone();
      copy2.transform(getRotationString(-rotation));
      copy2.attr('opacity','0')
      doRotationAnimation(copy2, rotation, 1, steps, duration ,function(){
        copy2.remove();
        thiss.bView.showActiveBunch();
        thiss.disable = false;
      });
      
      // so the animation doesn't interfere with the page turners
     thiss.bView.pageturner.sprite.toFront(); 
      
      // Hide next b's branches
      thiss.bView.branchSprites[b.activeBunch].hide();

      // Bring project box to front so that it's visible
      thiss.bView.projectBox.sprite.toFront();
    };

    var rightClickHandler = function(e) {
      var b = thiss.b;

      // Do nothing if right click shouldn't be possible
      if (b.activeBunch == b.myBranches.length - 1 || thiss.disable) {
        return;
      }

      // Disable turning until animation is over
      thiss.disable = true;

      // Clone b's current branches, animate them
      var sprite = thiss.bView.branchSprites[b.activeBunch];
      var copy = sprite.clone();
      var rotation = -30;
      var steps = 1;
      var duration = 400;
      doRotationAnimation(copy, rotation, 0, steps, duration, function() {copy.remove()});
      
      // Hide b's branches
      thiss.bView.branchSprites[b.activeBunch].hide();

      // Switch to b's next branches, clone and animate them. At the end of animation show real branch.
      b.activeBunch++;
      thiss.bView.showActiveBunch();
      sprite = thiss.bView.branchSprites[b.activeBunch];
      var copy2 = sprite.clone();
      copy2.transform(getRotationString(-rotation));
      copy2.attr('opacity','0')
      doRotationAnimation(copy2, rotation, 1, steps, duration ,function(){
        copy2.remove();
        thiss.bView.showActiveBunch();
        thiss.disable = false;
      });
      
      // so the animation doesn't interfere with the page turners
      thiss.bView.pageturner.sprite.toFront(); 
      
      // Hide next b's branches
      thiss.bView.branchSprites[b.activeBunch].hide();

      // Bring project box to front so that it's visible
      thiss.bView.projectBox.sprite.toFront();
    };

    // This needs to be implemented as a chain of animations rather than a single animation,
    // because as the change is a transformation, a linear interpolation is used for the animation.
    // If for example the rotation is 180 degrees, the branches invert across the origin rather than
    // doing any rotation.
    //
    // However, it looks reasonable to use one step for small angles.
    var doRotationAnimation = function(elem, rotation, opacity, steps, duration, callback, stepnum) {
      if (stepnum == undefined) 
        stepnum = 1;

      if (stepnum > steps) {
        callback();
        return;
      }

      var ease = "linear";
      if (steps == 1) // Use fancy ease if there's only 1 step
        ease = ">";

      var setOpacity
      if (opacity == 0) // opacity from 1 to 0
        setOpacity = 1-stepnum/steps;
      else if (opacity == 1) // opacity from 0 to 1
        setOpacity = stepnum/steps;
      else
        throw "Unsupported opacity goal";

      elem.animate({transform: getRotationString(rotation/steps), opacity: setOpacity},
        duration/steps,
        ease,
        function() {
            doRotationAnimation(elem, rotation, opacity, steps, duration, callback, stepnum+1);
        }
      );

    }

    var getRotationString = function(deg) {
      return 'r'+deg+','+posx+','+posy+'...';
    }

    var hoverInLeft = function(e) {
      thiss.leftHover = true;
      thiss.updateLargeText(b,'l');

      var puffW = w*scale*puffFactor;
      var puffH = h*scale*puffFactor;

      var svg = thiss.svg;
      thiss.leftLeaf.attr({
        fill: svg.hoverFillColor,
        stroke: svg.hoverStrokeColor
      });

      thiss.leftBoxes.toFront();
      thiss.box4.animate({        
        x:posx-puffW/2,
        y:posy-puffH/2,
        width:puffW,
        height:puffH,
      }, 100);
      thiss.box5.animate({        
        x:posx-puffW/2,
        y:posy-puffH/2,
        width:puffW,
        height:puffH,
      }, 100);
      thiss.box6.animate({        
        x:posx-puffW/2,
        y:posy-puffH/2,
        width:puffW,
        height:puffH,
      }, 100);
    };

    var hoverOutLeft = function(e) {
      thiss.leftHover = false;
      if(thiss.box4.isVisible())
        thiss.updateSmallText(b,'l');

      var svg = thiss.svg;
      thiss.leftLeaf.attr({
        fill: svg.fillColor,
        stroke: svg.strokeColor
      });

      thiss.box4.animate({
        x:-w/2*scale+posx,
        y:-h/2*scale+posy,
        width:w*scale,
        height:h*scale,
        r:2*scale
      }, 
      100);
      thiss.box5.animate({
        x:-w/2*scale+posx,
        y:-h/2*scale+posy,
        width:w*scale,
        height:h*scale,
        r:2*scale
      }, 
      100);
      thiss.box6.animate({
        x:-w/2*scale+posx,
        y:-h/2*scale+posy,
        width:w*scale,
        height:h*scale,
        r:2*scale
      }, 
      100);
    };

    var hoverInRight = function() {
      thiss.rightHover = true;
      thiss.updateLargeText(b,'r');

      var puffW = w*scale*puffFactor;
      var puffH = h*scale*puffFactor;

      var svg = thiss.svg;
      thiss.rightLeaf.attr({
        fill: svg.hoverFillColor,
        stroke: svg.hoverStrokeColor
      });

      thiss.rightBoxes.toFront();
      thiss.box1.animate({        
        x:posx-puffW/2,
        y:posy-puffH/2,
        width:puffW,
        height:puffH,
      }, 100);
      thiss.box2.animate({        
        x:posx-puffW/2,
        y:posy-puffH/2,
        width:puffW,
        height:puffH,
      }, 100);
      thiss.box3.animate({        
        x:posx-puffW/2,
        y:posy-puffH/2,
        width:puffW,
        height:puffH,
      }, 100);
    };

    var hoverOutRight = function() {
      thiss.rightHover = false;
      if(thiss.box1.isVisible())
        thiss.updateSmallText(b,'r');

      thiss.rightLeaf.attr({
        fill: svg.fillColor,
        stroke: svg.strokeColor
      });

      thiss.box1.animate({
        x:-w/2*scale+posx,
        y:-h/2*scale+posy,
        width:w*scale,
        height:h*scale,
        r:2*scale
      }, 
      100);
      thiss.box2.animate({
        x:-w/2*scale+posx,
        y:-h/2*scale+posy,
        width:w*scale,
        height:h*scale,
        r:2*scale
      }, 
      100);
      thiss.box3.animate({
        x:-w/2*scale+posx,
        y:-h/2*scale+posy,
        width:w*scale,
        height:h*scale,
        r:2*scale
      }, 
      100);
    };

    this.box6.node.onclick = leftClickHandler;
    this.box3.node.onclick = rightClickHandler;

    this.box3.hover( hoverInRight, hoverOutRight );
    this.box6.hover( hoverInLeft, hoverOutLeft );
  },

  update:function() {
    var b = this.b;
    this.updateText(b,'r');
    this.updateText(b,'l');
    if (b.activeBunch == 0) {
      this.leftLeaf.hide();
      this.leftBoxes.hide();
      this.rightLeaf.show();
      this.rightBoxes.show();
    } else if (b.activeBunch == b.myBranches.length - 1) {
      this.rightLeaf.hide();
      this.rightBoxes.hide();
      this.leftLeaf.show();
      this.leftBoxes.show();
    } else {
      this.leftLeaf.show();
      this.leftBoxes.show();
      this.rightLeaf.show();
      this.rightBoxes.show();
    }
  },

  updateText:function(b, side) {
    if (side == 'r' && this.rightHover || side == 'l' && this.leftHover) {
      this.updateLargeText(b, side);
    } else if (side == 'r' || side == 'l') {
      this.updateSmallText(b, side);
    } else {
      throw "side must be 'l' or 'r'";
      return;
    }
  },

  updateSmallText:function(b, side) {
    var t, txt;
    if (side == "r") {
      t = this.rightText;
      var projectsToRight = b.activeBunch == b.myBranches.length-1 ? 0 : b.numChildren-b.projectsToLeft[b.activeBunch+1];
      txt = projectsToRight;
    } else if (side == "l") {
      t = this.leftText;
      txt = b.projectsToLeft[b.activeBunch];
    } else {
      throw "Side must be 'l' or 'r'";
      return;
    }
    this.textPositioner(txt, side, t, this.w, this.h);

  },

  updateLargeText:function(b, side) {
    var t, txt;
    if (side == "r") {
      t = this.rightText;
      var projectsToRight = b.activeBunch == b.myBranches.length-1 ? 0 : b.numChildren-b.projectsToLeft[b.activeBunch+1];
      txt = projectsToRight+" more >";
    } else if (side == "l") {
      t = this.leftText;
      txt = "< "+b.projectsToLeft[b.activeBunch]+" more";
    } else {
      throw "Side must be 'l' or 'r'";
      return;
    }

    var puffFactor = this.svg.puffFactor;
    var scale = b.get('scale')*this.svg.falloff;
    var w = App.projectBoxSvg.w*scale*puffFactor;
    var h = App.projectBoxSvg.h*scale*puffFactor;

    this.textPositioner(txt, side, t, w, h);

  },

  textPositioner:function(txt, side, parentSprite, parentW, parentH) {
    // Remove old text
    while (parentSprite.length > 0) {
      parentSprite.pop().remove();
    }

    // Create new text. Resize so that it fits in box.
    var textPath = this.paper.print(0,0,txt,this.paper.getFont("NotoSans"),20,"baseline");
  	textPath.attr({
  		fill:this.svg.textColor
  	});
    $(textPath.node).css({'pointer-events': 'none'});
    var textOffsetX = textPath.getBBox().x;
    var textOffsetY = textPath.getBBox().y;
    var textOrigW = textPath.getBBox().width;
    var textOrigH = textPath.getBBox().height;
    parentSprite.push(textPath);
    var padding = this.padding;
    var textPathScale = Math.min((parentW-2*padding)/textOrigW,(parentH*2/3-padding)/textOrigH);

    // Calculate coordinates of new path
    var x, y;
    if (side == 'l') {
      // Left align
      x = this.posx-textOffsetX-parentW/2+padding;
      textPath.transform(this.leftTransform);
    } else if (side == 'r') {
      // Right align
      x = this.posx-textOffsetX+parentW/2-textOrigW*textPathScale-padding;
      textPath.transform(this.rightTransform);
    }

    y = this.posy-textOffsetY+parentH/2-textOrigH*textPathScale-padding; 
    textPath.transform('...t'+x+','+y);
    textPath.transform('...s'+textPathScale+','+textPathScale+','+(textOffsetX)+','+(textOffsetY));
  },

});